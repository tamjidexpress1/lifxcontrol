package com.example.tamjid.lifxcontroller;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by RumyAndroid on 7/28/2016.
 */
public class HttpManager {
    public static String getData(RequestPackage p){
        BufferedReader reader=null;
        String uri=p.getUri();
        if(p.getMethod().equals("GET")) {
            uri+=p.getEncodedParams();}
        try{
            URL url=new URL(uri);
            //HttpURLConnection con= (HttpURLConnection) url.openConnection();
            //con.setRequestMethod(p.getMethod());

            HttpURLConnection http = null;

            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                http = https;
                try {
                    InputStream in = new BufferedInputStream(https.getInputStream());
                    //readStream(in);
                } finally {
                    https.disconnect();
                }

            } else {
                http = (HttpURLConnection) url.openConnection();
            }

            return  null;

            /*StringBuilder sb =new StringBuilder();
            reader= new BufferedReader(new InputStreamReader(con.getInputStream()));

            String line;
            while((line=reader.readLine())!=null)
            {sb.append(line+"\n");
            }
            return sb.toString();*/
        }
        catch(Exception e){

            e.printStackTrace();
            return null;
        }
        finally{
            if(reader!=null){
                try{
                    reader.close();
                }
                catch(IOException e){
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


    // always verify the host - dont check for certificate
    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    /**
     * Trust every server - dont check for any certificate
     */
    private static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[] {};
            }

            public void checkClientTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }
        } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection
                    .setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}


